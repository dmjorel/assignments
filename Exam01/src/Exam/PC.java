package Exam;
// Question06
public class PC {
	
	public  void Dimensions(double a, double b, double c) {
		double width = a;
		double length = b;
		double depth = c;
	}
	
	public  void Case(String a, String b, int c) {
		String model = a;
		String manufacturer = b;
		int powerSupply = c;
	}
	
	public  void Monitor (String a, String b, int c) {
		String model = a;
		String manufacturer = b;
		int size = c;
	}
	
	public void Resolution(int a, int b) {
		
	}
	
	public void Motherboard (String a, String b, int c, int d, String e) {
		
	}
	
	public void pressPowerButton() {
		System.out.println("Power button pressed");
	}
	
	
	public static void main(String[] args) {
		
		Dimensions dimensions = new Dimensions(20, 20, 5);
		Case theCase = new Case("220B", "Dell", "240", dimensions);
		Monitor theMonitor = new Monitor("27inch Beast", "Acer", 27, new Resolution(2540, 1440));
		Motherboard theMotherboard = new Motherboard("BJ-200", "Asus", 4, 6, "v2.44");
		PC thePC = new PC(theCase, theMonitor, theMotherboard);
		thePC.getTheCase().pressPowerButton();

	}
}


