package Exam;

// Question04.01

public class Floor {
	public static double width;
	public static double length;
	public static int floor1;


	public Floor() {
		
	}

	public double getWidth() {
		return width;
	}

	public void setWidth(double width) {
		/*
		if (width < 0) {
			double width1 = 0;
		} */
		this.width = width;
	}

	public double getLength() {
		return length;
	}

	public void setLength(double length) {
		/*
		if (length < 0) {
			double length = 0; // wasn't sure why it wasn't working
		} */
		this.length = length;
	}

	public static double getArea() {

		double area = width*length;
		return area;
	}
	
	
	
	public static void main(String[] args) {
		
		Floor a = new Floor();
		
		a.setLength(30); //for example
		a.setWidth(35);
		
		System.out.println("The area of the floor is: " + getArea() + " meters squared");

	} 
	
}
