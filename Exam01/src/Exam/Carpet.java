package Exam;

// Question04.02

public class Carpet {
	
	static double cost;
	
	public Carpet () {
		
	}
	
	public static double getCost() {
		return cost;
	}

	public void setCost(double cost) {
		if (cost < 0 ) {
			cost = 0;
		}
		
		this.cost = cost;
	}
	

	
	public static void main(String[] args) {
		Carpet c = new Carpet();
		c.setCost(120.00);
		System.out.println("The cost of the carpet is: " + getCost() + "$.");
	} 
}
