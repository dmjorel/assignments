package Exam;

// Question05

public abstract class Bank {
	double amount;
	
	public abstract void getBalance(double amount);
	
	public static void main(String[] args) {
		
		BankA depositA = new BankA();
		BankB depositB = new BankB();
		BankC depositC = new BankC();
		
		depositA.getBalance(100.00);
		depositB.getBalance(150.00);
		depositC.getBalance(200.00);
		
	}

}

class BankA extends Bank {
	public void getBalance(double amount) {
		System.out.println("Your balance is: " + amount);
	}
}

class BankB extends Bank {
	public void getBalance(double amount) {
		System.out.println("Your balance is: " + amount);
	}
}

class BankC extends Bank {
	public void getBalance(double amount) {
		System.out.println("Your balance is: " + amount);
	}
}


