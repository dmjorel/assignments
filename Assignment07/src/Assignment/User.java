package Assignment;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Comparator;

//1- Create a Class User with the fields(int id, string name, java.time.LocalDate birthdate)
//2- Create a list of 5 users with test data.
//3- Create a method name it as sortListBytype(int type)
//4- based on the type sort them by id, by name and birthdate

class Users{
	private int id;
	private String name;
	private LocalDate birthyear;
	
	Users(int id, String name, LocalDate birthyear){
		this.id = id;
		this.name = name;
		this.birthyear = birthyear;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public LocalDate getBirthyear() {
		return birthyear;
	}

	public void setBirthyear(LocalDate birthyear) {
		this.birthyear = birthyear;
	}
	
	@Override
	public String toString() {
		return "User [ID = " + id + ", name = " + name + ", birthyear = " + birthyear + "]";
	}
}

public class User {

	public static void main(String[] args) {
		List<Users> userList= new ArrayList<Users>();
		userList.add(new Users(123, "A", LocalDate.parse("1986-07-06")));
		userList.add(new Users(405, "E", LocalDate.parse("1979-12-12")));
		userList.add(new Users(699, "C", LocalDate.parse("1990-10-01")));
		userList.add(new Users(495, "D", LocalDate.parse("1999-12-31")));
		userList.add(new Users(404, "B", LocalDate.parse("1994-04-04")));
		
		Collections.sort(userList, new IdComparator());
		System.out.println("after sort by Id");
		userList.forEach(usr -> System.out.println(usr));
		
		Collections.sort(userList, new NameComparator());
		System.out.println("after sort by Name");
		userList.forEach(usr -> System.out.println(usr));
		
		Collections.sort(userList, new DateComparator());
		System.out.println("after sort by birthdate");
		userList.forEach(usr -> System.out.println(usr));
		
		
		
	}

}
