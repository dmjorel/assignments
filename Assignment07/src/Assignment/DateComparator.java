package Assignment;

import java.time.LocalDate;
import java.util.Comparator;

public class DateComparator implements Comparator<Users>{

	@Override
	public int compare(Users o1, Users o2) {
		if (o1.getBirthyear() == o2.getBirthyear()) {
			return 0;
		}
		else if (o1.getBirthyear() > o2.getBirthyear()) {
			return 1;
		}
		else {
			return -1;
		}
	}

}
