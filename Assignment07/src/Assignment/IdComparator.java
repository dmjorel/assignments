package Assignment;

import java.util.Comparator;

public class IdComparator implements Comparator<Users>{

	@Override
	public int compare(Users o1, Users o2) {
		if (o1.getId() == o2.getId()) {
			return 0;
		}
		else if (o1.getId() > o2.getId()) {
			return 1;
		}
		else {
			return -1;
		}
	}

}
