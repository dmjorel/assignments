package Exam_02;

public class Address {
	
	private String streetAddress;
	private int buildingNumber;
	
	public Address(String streetAddress, int buildingNumber) {
		this.streetAddress = streetAddress;
		this.buildingNumber = buildingNumber;
	}

	public String getStreetAddress() {
		return streetAddress;
	}

	public int getBuildingNumber() {
		return buildingNumber;
	}

	public void setStreetAddress(String streetAddress) {
		this.streetAddress = streetAddress;
	}

	public void setBuildingNumber(int buildingNumber) {
		this.buildingNumber = buildingNumber;
	}

}
