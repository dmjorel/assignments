package Exam_02;

import java.io.FileNotFoundException;
import java.io.PrintWriter;

public class SendEmail implements ISendInfo {

	@Override
	public boolean validateMessage(User sender, User receiver, String body) {
		
		if (body.contains("^")) {
			throw new IllegalArgumentException("Message cannot contain this special character.");
		} else if (body.contains("*")) {
			throw new IllegalArgumentException("Message cannot contain this special character.");
		} else if (body.contains("!")) {
			throw new IllegalArgumentException("Message cannot contain this special character.");
		} else {
			return true;
		}
		/*
		try {
			body.contains("^");
			body.contains("*");
			body.contains("!");
		} catch (IllegalArgumentException e){
			System.out.println("Message cannot contain this special character.");
		}
		*/
	}

	@Override
	public void sendMessage(Message message) {
		PrintWriter out = null;
		try {
			out = new PrintWriter("email.txt");
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		out.println(message);
	}
	
}
