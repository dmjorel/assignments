package Exam_02;

public class EmailUser extends User{

	String emailAddress;
	
	public EmailUser (String firstName, String lastName, Address address, String emailAddress) {
		super(firstName, lastName, address);
		
		if (!emailAddress.contains("@") || !emailAddress.contains(".")) {
			throw new IllegalArgumentException("Email address must contain an @ and .");
		} else {
			this.emailAddress = emailAddress;
		}
	}
}
