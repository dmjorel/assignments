package Exam_02;

class SmsUser extends User{
	
	String phoneNumber;
	
	public SmsUser(String firstName, String lastName, Address address, String phoneNumber) {
		super(firstName, lastName,address);
		setPhoneNumber(phoneNumber);
	}
	
	public void setPhoneNumber(String phoneNumber) {
		
		if (phoneNumber.length() > 10) {
			throw new IllegalArgumentException("Phone number must not be more than 10 digits!");
		}

        for (char i : phoneNumber.toCharArray()) {
            if (!Character.isDigit(i)) {
                throw new IllegalArgumentException("Phone can only contain numbers.");
            }
        }

        this.phoneNumber = phoneNumber;
	}

}
