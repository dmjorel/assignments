package Exam_02;

import java.util.ArrayList;
import java.util.List;

public class App {

	public static void main(String[] args) {
		List<Message> listOfMessages = new ArrayList<Message>();
		
		EmailMessage emailMessage = new EmailMessage(
				new EmailUser("Judy", "Foster", new Address("Main Street", 1), "a.b@g.com"),
				new EmailUser("Betty", "Beans", new Address("Second Street", 2), "v.r@g.com"),
				"This is one* email");
				
		SmsMessage smsMessage = new SmsMessage(
				new SmsUser("Judy", "Foster", new Address("Main Street", 1), "123123"),
				new SmsUser("Betty", "Beans", new Address("Second Street", 2), "1232313"),
				"This is one* sms");
		
	
		listOfMessages.add(emailMessage);
		listOfMessages.add(smsMessage);
		
		System.out.println(emailMessage.toString());
		System.out.println(smsMessage.toString());
	}

}
