package Exam02;

public class Message {
	
	private User receiver;
	private User sender;
	private String body;
	
	public Message(User receiver, User sender, String body) {
		this.receiver = receiver;
		this.sender = sender;
		this.body = body;
	}

	public User getReceiver() {
		return receiver;
	}

	public User getSender() {
		return sender;
	}

	public String getBody() {
		return body;
	}

	public void setReceiver(User receiver) {
		this.receiver = receiver;
	}

	public void setSender(User sender) {
		this.sender = sender;
	}

	public void setBody(String body) {
		this.body = body;
	}

}
