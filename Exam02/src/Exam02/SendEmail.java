package Exam02;

import java.io.FileNotFoundException;
import java.io.PrintWriter;

public class SendEmail implements ISendInfo {

	@Override
	public boolean validateMessage(User sender, User receiver, String body) {
		
		// Im having difficulty trying to put restrictions on the emailAddress on this page
		// i had to do it in the EmailUser
		
		
		if (body.contains("^")) {
			throw new IllegalArgumentException("Message cannot contain this special character.");
		} else if (body.contains("*")) {
			throw new IllegalArgumentException("Message cannot contain this special character.");
		} else if (body.contains("!")) {
			throw new IllegalArgumentException("Message cannot contain this special character.");
		} else {
			return true;
		}
		
		/*
		if (body.matches("[^abcABC]")){
			throw new IllegalArgumentException("Message cannot contain this special character.");
		} else {
			return true;
		}
		*/

	}

	@Override
	public void sendMessage(Message message) {
		PrintWriter out = null;
		try {
			out = new PrintWriter("email.txt");
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		out.println(message);
	}
	
}
