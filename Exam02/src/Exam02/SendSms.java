package Exam02;

import java.io.FileNotFoundException;
import java.io.PrintWriter;

public class SendSms implements ISendInfo {

	@Override
	public boolean validateMessage(User sender, User receiver, String body) {
		
		// Im having difficulty trying to put restrictions on the phoneNumber on this page
		// i had to do it in the SmsUser
		
		if (body.length() == 0) {
			throw new IllegalArgumentException("Message must not be empty!");
		} else if(body.length() > 160) {
			throw new IllegalArgumentException("Message must not be more than 160 characters!");
		} else if(body.contains("&")) {
			throw new IllegalArgumentException("Message must not contain special characters!");
		} else if(body.contains("#")) {
			throw new IllegalArgumentException("Message must not contain special characters!");
		} else if(body.contains("@")) {
			throw new IllegalArgumentException("Message must not contain special characters!");
		} else {
			return true;
		}
		
		/*
		try {
			body.length() > 160 || body.length() <= 0;
			body.contains("&");
			body.contains("#");
			body.contains("@");
		} catch (IllegalArgumentException e){
			System.out.println("Message cannot contain this special character.");
		}
		*/
	}

	@Override
	public void sendMessage(Message message) {
		PrintWriter out = null;
		try {
			out = new PrintWriter("sms.txt");
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		out.println(message);
		
	}

}
