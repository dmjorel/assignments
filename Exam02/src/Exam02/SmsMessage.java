package Exam02;

public class SmsMessage extends Message{

	public SmsMessage(User receiver, User sender, String body) {
		super(receiver, sender, body);
	}

	@Override
	public String toString() {
		return "Sms Message to " + getReceiver() + ", from " + getSender() + ", message: "
				+ getBody();
	}

}
