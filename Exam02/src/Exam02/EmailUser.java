package Exam02;

import java.util.regex.Pattern;

public class EmailUser extends User{

	String emailAddress;
	
	public EmailUser (String firstName, String lastName, Address address, String emailAddress) {
		super(firstName, lastName, address);
		
		if (!emailAddress.contains("@") || !emailAddress.contains(".")) {
			throw new IllegalArgumentException("Email address must contain an @ and .");
		} else {
			this.emailAddress = emailAddress;
		}
		
		/*
		with regex it seems to not work properly, i don't know what I'm doing wrong
		if (Pattern.matches("@", emailAddress) || Pattern.matches(".", emailAddress)) {
			throw new IllegalArgumentException("Email address must contain an @ and .");
		} else {
			this.emailAddress = emailAddress;
		}
		*/
		
	}

	public String getEmailAddress() {
		return emailAddress;
	}

	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}

	@Override
	public String toString() {
		return "Email User " + getFirstName() + " " + getLastName();
	}
	
	
}
