package Exam02;

public class EmailMessage extends Message{

	public EmailMessage(User receiver, User sender, String body) {
		super(receiver, sender, body);
		
	}

	@Override
	public String toString() {
		return "Email Message to " + getReceiver() + ", from " + getSender() + ", message: "
				+ getBody();
	}

}
