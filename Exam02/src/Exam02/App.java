package Exam02;

import java.util.ArrayList;
import java.util.List;

public class App {

	public static void main(String[] args) {
		List<Message> listOfMessages = new ArrayList<Message>();
		
		EmailMessage emailMessage = new EmailMessage(
				new EmailUser("Judy", "Foster", new Address("Main Street", 1), "a.b@g.com"),
				new EmailUser("Betty", "Beans", new Address("Second Street", 2), "v.r@g.com"),
				"This is one email.");
				
		SmsMessage smsMessage = new SmsMessage(
				new SmsUser("Judy", "Foster", new Address("Main Street", 1), "123123"),
				new SmsUser("Betty", "Beans", new Address("Second Street", 2), "1232313"),
				"This is one sms.");
		/*
		EmailMessage emailMessage02 = new EmailMessage(
				new EmailUser("Emily", "Foster", new Address("Sixth Street", 1), "g.ng.com"),
				new EmailUser("Julie", "Beans", new Address("Fifth Street", 2), "lk@gcom"),
				"This is one * email");
				
		SmsMessage smsMessage02 = new SmsMessage(
				new SmsUser("Andre", "Foster", new Address("Third Street", 1), "123456g789"),
				new SmsUser("Marco", "Beans", new Address("fourth Street", 2), "65656565650"),
				"This is one # sms");
		*/
	
		listOfMessages.add(emailMessage);
		listOfMessages.add(smsMessage);
		//listOfMessages.add(emailMessage02);
		//listOfMessages.add(smsMessage02);
		
		System.out.println(emailMessage.toString());
		System.out.println();
		System.out.println(smsMessage.toString());
		System.out.println();
		//System.out.println(emailMessage02.toString());
		//System.out.println();
		//System.out.println(smsMessage02.toString());
		
		// emailMessage02 and smsMessage02 are with errors to try out the exceptions
		// i still wasn't comfortable with file, although i did my best

	}

}
