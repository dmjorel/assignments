package App;

abstract class Animals {
	
	public abstract void Sound();
	}	
}
class Cat extends Animals {
	public void Sound01() {
		System.out.println("Meow!");
	}
}
	
class Dog extends Animals {
	public void Sound02() {
		System.out.println("Bark!");
	}
}
public class Calculate {

	public static void main(String[] args) {
		Cat cat = new Cat();
		Dog dog = new Dog();
		cat.Sound01();
		dog.Sound02();
	}

}
