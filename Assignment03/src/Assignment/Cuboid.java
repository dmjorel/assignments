package Assignment;

public class Cuboid extends Rectangle {
	
	double height;
	
	public Cuboid(double width, double length, double height) {
		super(width, length);
		if (height < 0) {
			this.height = 0;
		}
		
		this.height = height;	// TODO Auto-generated constructor stub
	}
	
	public double getHeight() {
		return height;
	}

	public void setHeight(double height) {
		this.height = height;
	}

	
	
	public double getVolume() {
		double volume = width*length*height;
		return volume;
	}
	
	
}