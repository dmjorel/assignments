package Assignment;

public class Rectangle {
	
	double width;
	double length;
	
	public Rectangle(double width, double length) { //constructor - initialization
		if (width < 0) {
			width = 0;
		}
		if (length < 0) {
			length = 0;
		}
		
		this.width = width;
		this.length = length;
	}
	
	public double getWidth() {
		return width;
	}

	public void setWidth(double width) {
		this.width = width;
	}

	public double getLength() {
		return length;
	}

	public void setLength(double length) {
		this.length = length;
	}
	
	public double getArea() {
		double area = length * width;
		return area;
	}

	public static void main(String[] args) {
	
		Rectangle rectangle = new Rectangle(4, 15);
		System.out.println(rectangle.getArea());
		
		Cuboid cuboid = new Cuboid(4, 15, 10);
		System.out.println(cuboid.getVolume());
		

	}
	
}



