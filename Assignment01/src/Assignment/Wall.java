package Assignment;

public class Wall {
	
	double width;
	double heigth;
	
	public Wall(double a, double b) {
		if (a < 0) {
			a = 0;
		}
		this.width = a;
		if (b < 0) {
			b = 0;
		}
		this.heigth = b;
	}

	public double getWidth() {
		return width;
	}

	public void setWidth(double width) {
		if (width < 0) {
			width = 0;
		}
		this.width = width;
	}

	public double getHeigth() {
		return heigth;
	}

	public void setHeigth(double heigth) {
		if (heigth < 0) {
			heigth = 0;
		}
		this.heigth = heigth;
	}
	
	public double getArea() {
		double area = width * heigth;
		return area;
	}

	public static void main(String[] args) {
		
		Wall wall = new Wall(5.0, 4.0);
		System.out.println("area = " + wall.getArea());
		wall.setWidth(10.4);
		wall.setHeigth(-1.5);
		System.out.println("width = " + wall.getWidth());
		System.out.println("height = " + wall.getHeigth());
		System.out.println("area = " + wall.getArea());
	}

}
