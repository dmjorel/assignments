package Assignment;

public class Person {
	
	String firstName;
	String lastName;
	int age;
	
	/*
	public Person (String a, String b, int c) {
		this.firstName = a;
		this.lastName = b;
		this.age = c;
	}
	*/
	
	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		if (age < 0) {
			age = 0;
		}
		this.age = age;
	}
	
	public boolean isTeen(int age) {
		if (age < 20 && age > 12) {
			return true;
		} else {
			return false;
		}
		
	}
	
	public String getFullName() {
		String fullName = firstName + " " + lastName;
		return fullName;
	}
	
	public static void main(String[] args) {
		
		Person person = new Person();
		person.setFirstName("Diego");
		person.setLastName("Alayo");
		person.setAge(10);
		System.out.println("full name = " + person.getFullName());
		System.out.println("Teen = " + person.isTeen(10));
		

	}

}