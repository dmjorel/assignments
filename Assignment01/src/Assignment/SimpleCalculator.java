package Assignment;

public class SimpleCalculator {
	
	double firstNumber;
	double secondNumber;

	public double getFirstNumber() {
		return firstNumber;
	}

	public void setFirstNumber(double firstNumber) {
		this.firstNumber = firstNumber;
	}

	public double getSecondNumber() {
		return secondNumber;
	}

	public void setSecondNumber(double secondNumber) {
		this.secondNumber = secondNumber;
	}
	
	public double getAdditionResult() {
		double addition = firstNumber + secondNumber;
		return addition;
	}
	
	public double getSubstractionResult() {
		double subtraction = firstNumber - secondNumber;
		return subtraction;
	}
	
	public double getMultiplicationResult() {
		double multiplication = firstNumber * secondNumber;
		return multiplication;
	}
	
	public double getDivisionResult() {
		if (secondNumber == 0) {
			return 0;
		}
		double division = firstNumber / secondNumber;
		return division;
	}
	
	
	public static void main(String[] args) {
		
		SimpleCalculator calculator = new SimpleCalculator();
		calculator.setFirstNumber(5.0);
		calculator.setSecondNumber(0);
		System.out.println("add = " + calculator.getAdditionResult());
		System.out.println("substract = " + calculator.getSubstractionResult());
		System.out.println("Multiply = " + calculator.getMultiplicationResult());
		System.out.println("Divide = " + calculator.getDivisionResult());

	}

}
