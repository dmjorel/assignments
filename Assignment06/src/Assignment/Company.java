package Assignment;

// question 2 = the hashcodes and methods are
// unique to every employee, we cannot use 
// the same ones if they are all different

// question 3 = interface when using for
// small bits of functionality, abstract
// when using large functional units

class Employee {
	
	String name;
	String ssn;
	
	Employee(String name, String ssn){
		this.name = name;
		this.ssn = ssn;
	}
	
	public void salary() {
		
	}
	
	public String toString() {
		return "Employee: " + name;
	}
}

class CommissionEmployee extends Employee {
	
	double sales;
	double commission;
	
	CommissionEmployee(String name, String ssn, double sale, double commission) {
		super(name, ssn);
		this.sales = sales;
		this.commission = commission;
	}
	
	public void salary() {
		
	}
	
	public String toString() {
		return "Employee: " + name +  ", your salary is: " + commission + " per day.";
	}
}

class HourlyEmployee extends Employee {
	
	double wage;
	double hours;
	
	HourlyEmployee(String name, String ssn, double wage, double hours){
		super(name, ssn);
		this.wage = wage;
		this.hours = hours;
		
	}
	
	public void salary() {
		
	}
	
	public String toString() {
		return "Employee: " + name + ", your salary is: " + wage + " per hour.";
	}
}

class SalariedEmployee extends Employee {
	
	double basicSalary;
	
	SalariedEmployee(String name, String ssn, double basicSalary){
		super(name, ssn);
		this.basicSalary = basicSalary;
	}
	
	public void salary() {
		
	}
	
	public String toString() {
		return "Employee: " + name + ", your salary is: " + basicSalary + " per hour.";
	}
	
}

public class Company {
	public static void main(String[] args) {
		
		Employee payRoll = new Employee("diego", "1224");
		
		payRoll.
		

	}
}
