package Assignment;
//question 4
class withSeeds {
	public void eat() {
		System.out.println("Start eating.");
	}
	
}

class peel extends withSeeds {
	public void eat() {
		System.out.println("Peel the fruit first.");
	}
	
	public void munch() {
		System.out.println("Break the shell first.");
	}
}



public class Fruits {
	
	public static void main(String[] args) {
		withSeeds melon = new withSeeds();
		peel coconut = new peel();
		
		melon.eat();
		coconut.eat();
		coconut.munch();
		
		// upcasting
		withSeeds banana = new peel();
		banana.eat(); 
		// error: banana.munch();
		
		// down casting
		withSeeds orange = new peel();
		peel apple = (peel)orange;
		apple.eat();
		apple.munch();
		
	}
	
}
